    document.addEventListener('DOMContentLoaded', function(){
    const list=[];
    const form = document.querySelector('.Attractions-list-form');
    const titleInput = document.querySelector('.Attractions-list-form-title-input');
    const description = document.querySelector('.Attractions-list-form-description-input');
    const items = document.querySelector('.title-input');

    form.addEventListener('submit', AddItemToList);
    function AddItemToList(event){
        event.preventDefault();

        const titleInput = event.target["Title"].value;
        const description = event.target["Description"].value;

        if(titleInput  != "" && description != "" ){
            const item = {
                titulo: titleInput,
                descricao: description,
    
            };
            list.push(item);
            renderListForm();
            resetInputs();
            console.log(list);
        }
   
}
    function renderListForm(){
        let itemsStructure = ""

        list.forEach(function(item){
            itemsStructure += ` 

                <li class="title-input1">
            
                       <span>
                            ${item.titulo}   
                       </span> 
                       <span>
                        ${item.descricao} 
                        </span>
                </li>
            
            `;

        })
        items.innerHTML = itemsStructure;
        items.innerHTML = itemsStructure;

        
        }
        function resetInputs(){
            titleInput.value = "";
            description.value = "";
    }

});